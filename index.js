function get_info() {
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.5) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            setTimeout(() => {
                resolve(tmp_id);
            }, timer);
        } else {
            setTimeout(() => {
                reject('Failed');
            }, timer);
        }
    });
}

function get_firstname() {
    first_name_list = ['','Adam', 'Eric', 'Peter'];
     return new Promise((resolve, reject) =>{
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            let pick = Math.ceil(Math.random()*3);
            setTimeout(() => {
                resolve(first_name_list[pick]);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
    // TODO : generate a success rate
    
    // TODO : generate a timer

    // TODO : random select a item from list
}

function get_lastname() {
    last_name_list = [' ','Jones', 'Smith', 'Johnson'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) =>{
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            let pick = Math.ceil(Math.random()*3);
            setTimeout(() => {
                resolve(last_name_list[pick]);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_username() {
    username_list = ['','Toyz', 'Faker', 'Dinter'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) =>{
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            let pick = Math.ceil(Math.random()*3);
            setTimeout(() => {
                resolve(username_list[pick]);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_email() {
    email_list = ['','asdf@google.com', 'qwer@microsoft.com', 'zxcv@cs.nthu.edu.tw'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) =>{
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            let pick = Math.ceil(Math.random()*3);
            setTimeout(() => {
                resolve(email_list[pick]);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_address() {
    address_list = ['','1027 Alpha Avenue', '3132 Kidd Avenue', '876 Jefferson Street'];

    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) =>{
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            let pick = Math.ceil(Math.random()*3);
            setTimeout(() => {
                resolve( address_list[pick]);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function initApp() {
    var reSamplebtn = document.getElementById('resamplebtn');
    reSamplebtn.addEventListener('click', retrieve_data);
}

async function retrieve_data() {
    var txtInfoName = document.getElementById('user-info-name');
    var txtFirstName = document.getElementById('firstName');
    var txtLastName = document.getElementById('lastName');
    var txtUserName = document.getElementById('username');
    var txtEmail = document.getElementById('email');
    var txtAddress = document.getElementById('address');
    var boxReSample = document.getElementById('re-sample');
    txtInfoName.innerText = '_';
    txtFirstName.value = '-';
    txtLastName.value = '-';
    txtUserName.value = '-';
    txtEmail.value = '-';
    txtAddress.value = '-';
    //console.log(typeof boxReSample.checked);
    /*if(boxReSample.checked == true){
        console.log("congr");
    }
    else{
        console.log("fuck");
    }*/

    try {
        // TODO : get_info first
        await get_info().then((fast)=>{
            txtInfoName.innerText = fast +'s'+ ' '+ 'information';
        }, (fal) => {
            txtInfoName.innerText = fal;
        });

        
        // TODO : call other function to get other data
        if(txtInfoName.innerText!='Failed'){
            var qq1 = get_firstname();
            var qq2 = get_lastname();
            var qq3 = get_username();
            var qq4 = get_email();
            var qq5 = get_address();
            Promise.all([qq1, qq2, qq3, qq4, qq5]).then(values => { 
                txtFirstName.value = values[0];
                txtLastName.value = values[1];
                txtUserName.value = values[2];
                txtEmail.value = values[3];
                txtAddress.value = values[4];
            }).catch(errrorMessage =>{
                txtInfoName.innerText = 'Failed';
                txtFirstName.value = '-';
                txtLastName.value = '-';
                txtUserName.value = '-';
                txtEmail.value = '-';
                txtAddress.value = '-';
            });
        }
        /*get_firstname().then((Name)=>{
            txtFirstName.value = Name;
        }, (fail)=>{
            txtFirstName.value = fail;
        });
        get_lastname().then((Name)=>{
            txtLastName.value = Name;
        }, (fail)=>{
            txtLastName.value = fail;
        });
        get_username().then((Name)=>{
            txtUserName.value = Name;
        }, (fail)=>{
            txtUserName.value = fail;
        });
        get_email().then((Name)=>{
            txtEmail.value = Name;
        }, (fail)=>{
            txtEmail.value = fail;
        });
        get_address().then((Name)=>{
            txtAddress.value = Name;
        }, (fail)=>{
            txtAddress.value = fail;
        });*/
        
    } catch (e) {
        
    }
}

window.onload = function() {
    initApp();
}